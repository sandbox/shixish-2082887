<?php
define('ER_REPORTING_URI', 'export');

define('ER_EXPORTS_ACCESS_PERMISSION','access nsf content');
define('ER_EXPORTS_DOWNLOAD_PERMISSION','download nsf content');

define('ER_EXPORTS_MODULE_DIR', drupal_get_path('module', 'er'));
  
/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the er module
 */
function er_exports_permission(){
	return array(
		ER_EXPORTS_ACCESS_PERMISSION=>array(
			'title' => t('Access EPSCoR NSF Reports')
		),
		ER_EXPORTS_DOWNLOAD_PERMISSION=>array(
			'title' => t('Access EPSCoR NSF Reporting Excel Tables')
		),
	);
} // function er_exports_perm()
  
/**
 * Menu for this module
 * @return array An array with this module's settings.
 */
function er_exports_menu() {
	$items = array();
  $items['admin/epscor'] = array(
		'menu_name' => 'management',
		'title' => 'EPSCoR',
		'description' => 'EPSCoR Reporting',
		'page callback' => 'system_admin_menu_block_page',
		'access arguments' => array('access administration pages'),
		'file' => 'system.admin.inc',
		'file path' => drupal_get_path('module', 'system'),
		'weight' => '-49',
	);
  
  //NSF TABLE VIEWS:
	$items['admin/epscor/views/salary-support'] = array(
    'menu_name' => 'management',
    'title' => 'NSF Table A - Salary Support',
    'description' => 'This view aims to help you determine where the Salary Support information is coming from.',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/salary-support'),
    'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
  );
	$items['admin/epscor/views/participants'] = array(
    'menu_name' => 'management',
    'title' => 'NSF Table B - Participants',
    'description' => 'This view aims to help you determine where the Participant information is coming from.',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/participants'),
    'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
  );
	$items['admin/epscor/views/collaborations'] = array(
    'menu_name' => 'management',
    'title' => 'NSF Table C - Collaborations',
    'description' => 'This view aims to help you determine where the Collaborator counts are coming from.',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/collaborations'),
    'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
  );
	$items['admin/epscor/views/external-engagement'] = array(
    'menu_name' => 'management',
    'title' => 'NSF Table D - External Engagements',
    'description' => 'This view aims to help you determine where the External Engagement counts are coming from.',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/external-engagement'),
    'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
  );
	$items['admin/epscor/views/outputs'] = array(
    'menu_name' => 'management',
    'title' => 'NSF Table E - Outputs',
    'description' => 'This view aims to help you determine where the Outputs counts are coming from.',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/outputs'),
    'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
  );
  
  $items['admin/epscor/reporting/download'] = array(
    'menu_name' => 'management',
    'title' => 'Download Reporting Tables',
		'type' => MENU_NORMAL_ITEM,
		'page callback' => 'drupal_goto',
		'page arguments' => array(ER_REPORTING_URI.'/excel/download'),
    'access arguments' => array(ER_EXPORTS_DOWNLOAD_PERMISSION),
  );
  
	// For the individual pages we do not want to have to define a menu item for
	// each one, so we can use a wildcard loader argument (the % item). This will
	// call the function er_exports_page_load() with the argument passed in via the URL.
	// So if someone enters the url reporting/collaborations, then Drupal will call
	// er_exports_page_load("collaborations"). If that returns false, the response will
	// be 404 not found. If it returns a value (in this case our page class) that
	// will be passed to our page function.
	$items[ER_REPORTING_URI.'/%'] = array(
		'title' => 'View Details',
		'description' => 'View reporting details',
		'page callback' => 'er_exports_page',
		'page arguments' => array(1),
		'access arguments' => array(ER_EXPORTS_ACCESS_PERMISSION),
		'type' => MENU_CALLBACK,
	);

	// We also want a download entry for each item, in case we want to allow
	// certain users to see the summaries, but not to download the excel product.
	$items[ER_REPORTING_URI.'/%/download'] = array(
		'title' => 'Download Details',
		'description' => 'Download reporting details.',
		'page callback' => 'er_exports_page_download',
		'page arguments' => array(1),
		'access arguments' => array(ER_EXPORTS_DOWNLOAD_PERMISSION),
		'type' => MENU_CALLBACK,
	);
	return $items;
}

//initializes, then returns an instance of a er_exports_page object which will be defined in an "inc" file ex: /modules/er/pages/_____.inc
function er_exports_load_page($arg){
	require_once ER_MODULE_DIR.'/er_exports_page.inc';
	if (module_load_include('inc', 'er_exports', 'pages/'.$arg)!==false){
		// Remember that our classes are prefixed with er_exports_
		$class = 'er_exports_' . str_replace( "-" , "_" , $arg);
		$class = new $class();
		return $class;
	}
	return false;
}

//this handles page loading, it uses er_exports_load_page to retrieve the page object which is used to display the page.
function er_exports_page($arg) {
	//dsm($arg, 'loading page');
	if($page = er_exports_load_page($arg)){
		// Set the title of the page. the drupal_set_title function takes care
		// of letting the theme know what to put in the TITLE and H1 tags.
		drupal_set_title($page->title());
	
		// Return the HTML content of the page
		return $page->html();
	}
	return drupal_not_found();
}

//menu hook which procs when trying to download certain excel sheets, ex: /reporting/participants/download
function er_exports_page_download($arg) {
	if (($library = libraries_detect('PHPExcel')) && !empty($library['installed']) && libraries_load_files($library)) {// The library is installed, and files are loaded.
		if($page = er_exports_load_page($arg)){
			$objPHPExcel = $page->excel();
			$objPHPExcel->setActiveSheetIndex(0); //initialize to the first page...
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			//comment these next few blocks if you want to debug the excel system... (shows error messeges)
			if (!ER_DEBUG_EXCEL){
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$page->title().'.xls"');
				header('Cache-Control: max-age=0');
				$objWriter->save('php://output');
				module_invoke_all('exit');
				exit;
			}
			return '';
		}
	}else {//Libraries had a problem loading PHPExcel...
		$error = $library['error'];// This contains a short status code of what went wrong, such as 'not found'.
		$error_message = $library['error message'];// This contains a detailed (localized) error message.
		drupal_set_message('PHPExcel library error: ' . $error_message, 'error');
		watchdog('er', $error_message, NULL, WATCHDOG_CRITICAL);
	}
	return drupal_not_found();
}